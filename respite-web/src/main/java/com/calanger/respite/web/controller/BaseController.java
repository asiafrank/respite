package com.calanger.respite.web.controller;

import com.calanger.common.util.UrlUtils;
import com.calanger.common.web.util.RequestUtils;
import com.calanger.respite.web.constant.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseController {
    public final static Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    @ModelAttribute("baseDomain")
    public String getBaseDomain() {
        return Constants.BASE_DOMAIN;
    }

    @ModelAttribute("staticDomain")
    public String getStaticDomain() {
        return Constants.STATIC_DOMAIN;
    }

    @ModelAttribute("homeDomain")
    public String getHomeDomain() {
        return Constants.HOME_DOMAIN;
    }

    @ModelAttribute("returnUrl")
    public String getReturnUrl() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return RequestUtils.getRequestURIWithQueryString(request);
    }

    @ModelAttribute("encodedReturnUrl")
    public String getEncodedReturnUrl() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return UrlUtils.encode(RequestUtils.getRequestURIWithQueryString(request));
    }
}