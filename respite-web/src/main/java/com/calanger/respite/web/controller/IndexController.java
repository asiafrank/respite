package com.calanger.respite.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController extends BaseController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("location", "index");
        return "/index";
    }

    @RequestMapping(value = "/content/{id}", method = RequestMethod.GET)
    public String index(@PathVariable Integer id, Model model) {
        if (id == null || id.equals(0)) {
            id = 0;
        } else if (id > 5) {
            id = 5;
        }
        model.addAttribute("location", id.toString());
        return "/content-" + id;
    }
}