package com.calanger.respite.web.constant;

import com.calanger.common.util.Config;

public final class Constants {
    public static final String BASE_DOMAIN;
    public static final String STATIC_DOMAIN;
    public static final String HOME_DOMAIN;

    static {
        BASE_DOMAIN = Config.getConfig().get("base.domain");
        STATIC_DOMAIN = Config.getConfig().get("static.domain");
        HOME_DOMAIN = Config.getConfig().get("home.domain");
    }

    private Constants() {
    }
}