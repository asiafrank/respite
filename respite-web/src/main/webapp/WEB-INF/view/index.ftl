<#import "/macro/url.ftl" as url>
<#import "/macro/js.ftl" as js>
<#import "/macro/css.ftl" as css>
<#escape x as x?html>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>首页</title>
<@css.app/>
<@css.fullPage/>
</head>
<body>
<#include "/common/header.ftl"/>

<div id="full-pages" class="full-pages">
  <div class="section s1 active">
    <div class="inner">
      <div class="ct clearfix">
        <h1>家庭档案管理：就医安排&疾病管理</h1>
        <div class="img-base">
          <img src="/assets/images/section-1.png" alt="">
        </div><!-- /.img-base -->
        <div class="desc">
          <ul class="list">
            <li class="item"><span class="ic ic-checkbox"></span><span>家庭健康档案的建立与管理</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>空巢老人档案的建立与管理</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>导医安排</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>疾病管理</span></li>
          </ul>
          <a class="desc-button" href="#">查看更多>></a>
        </div><!-- /.desc -->
      </div><!-- /.ct.clearfix -->
    </div><!-- /.inner -->
  </div><!-- /.section -->

  <div class="section s2">
    <div class="inner">
      <div class="ct">
        <div class="img-base">
          <img src="/assets/images/section-2.png" alt="">
        </div><!-- /.img-base -->
        <h2>老人日常照料内容</h2>
        <div class="circle"></div>
        <div class="desc">
          <ul class="list">
            <li class="item"><span class="ic ic-checkbox"></span><span>生活护理指导培训</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>呼吸系统护理</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>哮喘恢复期护理</span></li>
          </ul>
          <a class="desc-button" href="#">查看更多>></a>
        </div><!-- /.desc -->
      </div><!-- /.ct.clearfix -->
    </div><!-- /.inner -->
  </div><!-- /.section -->

  <div class="section s3">
    <div class="inner">
      <div class="ct">
        <div class="img-base">
          <img src="/assets/images/section-3.png" alt="">
        </div><!-- /.img-base -->
        <h2>家庭护理服务</h2>
        <div class="circle"></div>
        <div class="desc">
          <ul class="list clearfix">
            <li class="item"><span class="ic ic-checkbox"></span><span>慢性病服务</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>化疗患者的护理</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>老年痴呆服务</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>偏瘫病人的康复护理</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>糖尿病人的服务</span></li>
            <li class="item"><span class="ic ic-checkbox"></span><span>老年人心理护理</span></li>
          </ul>
          <a class="desc-button" href="#">查看更多>></a>
        </div><!-- /.desc -->
      </div><!-- /.ct.clearfix -->
    </div><!-- /.inner -->
  </div><!-- /.section -->

  <div class="section s4">
    <div class="inner">
      <div class="ct">
        <h2>病友俱乐部</h2>
        <div class="desc">
          <p class="txt">养老休闲，海外健康服务</p>
          <a class="desc-button" href="#">查看更多>></a>
        </div><!-- /.desc -->

        <div class="img-base">
          <ul>
            <li class="item"><img src="/assets/images/s4-img-1.jpg" alt=""></li>
            <li class="item m"><img src="/assets/images/s4-img-2.jpg" alt=""></li>
            <li class="item"><img src="/assets/images/s4-img-3.jpg" alt=""></li>
          </ul>
        </div><!-- /.img-base -->
        <div class="circle"></div>
      </div><!-- /.ct.clearfix -->
    </div><!-- /.inner -->
  </div><!-- /.section -->

  <div class="section s5">
    <div class="inner">
      <div class="ct">
        <h2>临终关怀</h2>
        <div class="desc">
          <p class="txt">做好临终关怀护理,让临终病人能舒适、安详、有尊严地走完人生最后路程,是医护人员应尽的责任,也是开展临终关怀的宗旨。</p>
          <a class="desc-button" href="#">查看更多>></a>
        </div><!-- /.desc -->

        <div class="img-base">
          <ul>
            <li class="item"><img src="/assets/images/s5-img-1.jpg" alt=""></li>
            <li class="item m"><img src="/assets/images/s5-img-2.jpg" alt=""></li>
            <li class="item"><img src="/assets/images/s5-img-3.jpg" alt=""></li>
          </ul>
        </div><!-- /.img-base -->
        <div class="circle"></div>
      </div><!-- /.ct.clearfix -->
    </div><!-- /.inner -->
  </div><!-- /.section -->

  <div class="section footer fp-auto-height">
    <#include "/common/footer.ftl"/>
  </div><!-- /.section -->
</div><!-- /.full-pages -->

</body>
<@js.jquery/>
<@js.fullPage/>
<script>
  $(function(){
    $('#full-pages').fullpage({
      verticalCentered: !1,
      navigation: !0
    });
  });
</script>
</html>
</#escape>