<#macro app>
<script src="<@url.static/>/assets/js/app.js?v=2016022601"></script>
</#macro>
<#macro jquery>
<script src="<@url.static/>/assets/js/jquery-1.8.3.min.js?v=2016022601"></script>
</#macro>
<#macro fullPage>
<script src="<@url.static/>/assets/js/jquery.fullPage.min.js?v=2016022601"></script>
</#macro>