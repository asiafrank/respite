<#macro app>
<link rel="stylesheet" href="<@url.static/>/assets/css/app.css?v=2016022601">
</#macro>
<#macro fullPage>
<link rel="stylesheet" href="<@url.static/>/assets/css/fullPage.css?v=2016022601">
</#macro>
<#macro slider>
<link rel="stylesheet" href="<@url.static/>/assets/css/slider.css?v=2016022601">
</#macro>