<#macro pagination>
<#escape x as x?html>
    <#assign paging_pre = 1>
     <#assign paging_next = 1>
    <#if (p.lastPageNumber == p.firstPageNumber)>
      <#assign paging_pre = p.firstPageNumber>
      <#assign paging_next = p.lastPageNumber>
    </#if>
    <#if (p.pageNumber &gt; 1) >
      <#assign paging_pre = (p.pageNumber - 1)>
    </#if>
    <#if (p.pageNumber &lt; p.lastPageNumber) >
      <#assign paging_next = (p.pageNumber+1)>
    </#if>
</#escape>
</#macro>