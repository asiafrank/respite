<#import "/macro/url.ftl" as url>
<#import "/macro/js.ftl" as js>
<#import "/macro/css.ftl" as css>
<#escape x as x?html>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>老人日常照料</title>
<@css.app/>
</head>
<body>
<#include "/common/header.ftl"/>

<div class="banner">
  <div class="inner">
    <div class="pic"></div>
    <div class="desc">
      <span class="ic ic-telephone"></span>
      <span class="txt">服务热线：400-888-8888</span>
    </div><!-- /.desc -->
  </div><!-- /.inner -->
</div><!-- /.banner -->

<div class="summary">
  <div class="inner">
    <div class="title">
      <span class="txt">家庭护理服务</span>
    </div><!-- /.title -->

    <div class="content mt-80">
      <div class="list type-b clearfix">
        <div class="item">
          <div class="ic ic-pic-08"></div>
          <p class="txt">慢性病服务</p>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-09"></div>
          <p class="txt">老年痴呆服务</p>
          <div class="info">
            <p>痴呆病人的家庭评估（BSSD)及居家照料</p>
          </div>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-10"></div>
          <p class="txt">糖尿病人的服务</p>
          <div class="info">
            <p>①饮食护理：建议每天至少三餐</p>
            <p>②运动护理：鼓励患者持之以恒，循序渐进的运动。</p>
            <p>③足护理：</p>
            <p>④糖尿病人用药指导，血压血糖每日监测</p>
          </div>
        </div><!-- /.item -->
      </div><!-- /.list -->

      <div class="list type-b mt-40 clearfix">
        <div class="item">
          <div class="ic ic-pic-11"></div>
          <p class="txt">化疗患者的护理</p>
          <div class="info">
            <p>向患者讲解化疗中可能出现的反应及处理<br>方法及预防措施，消除心理障碍。严格无<br>菌操作，避免交叉感染。紫外线消毒。</p>
            <p>①化疗期间及化疗后预防感染服务的指导</p>
            <p>②护理指导</p>
            <p>③潜在的并发症</p>
          </div>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-12"></div>
          <p class="txt">偏瘫病人的康复护理</p>
          <div class="info">
            <p>并发症康复护理</p>
          </div>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-13"></div>
          <p class="txt">老年人心理护理</p>
          <div class="info">
            <p>①沟通及指导</p>
            <p>②专业量表检测：焦虑量表</p>
          </div>
        </div><!-- /.item -->
      </div><!-- /.list -->
    </div><!-- /.content -->
  </div><!-- /.inner -->
</div><!-- /.summary -->

<#include "/common/footer.ftl"/>
</body>
</html>
</#escape>