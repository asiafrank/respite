<#import "/macro/url.ftl" as url>
<#import "/macro/js.ftl" as js>
<#import "/macro/css.ftl" as css>
<#escape x as x?html>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>老人日常照料</title>
<@css.app/>
</head>
<body>
<#include "/common/header.ftl"/>

<div class="banner">
  <div class="inner">
    <div class="pic"></div>
    <div class="desc">
      <span class="ic ic-telephone"></span>
      <span class="txt">服务热线：400-888-8888</span>
    </div><!-- /.desc -->
  </div><!-- /.inner -->
</div><!-- /.banner -->

<div class="summary">
  <div class="inner">
    <div class="title">
      <span class="txt">老人日常照料内容</span>
    </div><!-- /.title -->

    <div class="content mt-80">
      <div class="list type-b clearfix">
        <div class="item">
          <div class="ic ic-pic-05"></div>
          <p class="txt">生活护理指导培训</p>
          <div class="info">
            <p>①压疮预防 ②人工排便</p>
            <p>③口腔、皮肤清洁护理</p>
          </div>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-06"></div>
          <p class="txt">呼吸系统护理</p>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-07"></div>
          <p class="txt">哮喘恢复期护理</p>
          <div class="info">
            <p>①饮食护理；②保证充足的睡眠；</p>
            <p>③调节室内的温度与湿度；</p>
            <p>④预防继发感染;坚持长期治疗。</p>
          </div>
        </div><!-- /.item -->
      </div><!-- /.list -->
    </div><!-- /.content -->
  </div><!-- /.inner -->
</div><!-- /.summary -->

<#include "/common/footer.ftl"/>
</body>
</html>
</#escape>