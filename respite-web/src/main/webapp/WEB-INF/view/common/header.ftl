<#escape x as x?html>
<div class="nav">
  <div class="inner clearfix">
    <div class="n-left">
      <span class="logo"></span>
    </div>
    <ul class="n-middle">
      <li class="item ${(location?? && location == "index")?string("active","")}"><a href="/">首页</a></li>
      <li class="item ${(location?? && location == "1")?string("active","")}"><a href="/content/1">家庭档案管理</a></li>
      <li class="item ${(location?? && location == "2")?string("active","")}"><a href="/content/2">老人日常照料</a></li>
      <li class="item ${(location?? && location == "3")?string("active","")}"><a href="/content/3">家庭护理服务</a></li>
      <li class="item ${(location?? && location == "4")?string("active","")}"><a href="/content/4">病友俱乐部</a></li>
      <li class="item ${(location?? && location == "5")?string("active","")}"><a href="/content/5">临终关怀</a></li>
    </ul>
    <div class="n-right">
      <a class="btn btn-orange" href="#">登录</a>
      <a class="btn btn-orange-hollow" href="#">注册</a>
    </div>
  </div><!-- /.inner -->
</div><!-- /.nav -->
</#escape>