<#escape x as x?html>
<div class="site-footer">
  <p class="txt">Copyright © 2016 喘息服务　锐司巴特（杭州）标准技术服务有限公司　浙ICP备8888888号</p>
  <p class="links">
    <a href="#">关于我们</a>
    <span class="separator">|</span>
    <a href="#">联系我们</a>
    <span class="separator">|</span>
    <a href="#">合作伙伴</a>
  </p>
</div><!-- /.footer -->
</#escape>