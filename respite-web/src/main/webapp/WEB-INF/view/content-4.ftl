<#import "/macro/url.ftl" as url>
<#import "/macro/js.ftl" as js>
<#import "/macro/css.ftl" as css>
<#escape x as x?html>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>老人日常照料</title>
<@css.app/>
<@css.slider/>
</head>
<body>
<#include "/common/header.ftl"/>

<div class="banner">
  <div class="inner">
    <div class="pic"></div>
    <div class="desc">
      <span class="ic ic-telephone"></span>
      <span class="txt">服务热线：400-888-8888</span>
    </div><!-- /.desc -->
  </div><!-- /.inner -->
</div><!-- /.banner -->

<div class="summary">
  <div class="inner">
    <div class="title">
      <span class="txt">病友俱乐部</span>
    </div><!-- /.title -->

    <p class="sub-title">养老休闲，海外健康服务</p>

    <div class="content mt-40">
      <div class="slider">
        <div class="prev" style="display: none;"></div>
        <div class="next" style="display: none;"></div>
        <div class="visible">
          <div class="wrapper">
            <div class="item">
              <img src="/assets/images/slider-img-1.jpg" alt="">
            </div><!-- /.item -->

            <div class="item">
              <img src="/assets/images/slider-img-1.jpg" alt="">
            </div><!-- /.item -->

            <div class="item">
              <img src="/assets/images/slider-img-1.jpg" alt="">
            </div><!-- /.item -->

            <div class="item">
              <img src="/assets/images/slider-img-1.jpg" alt="">
            </div><!-- /.item -->

            <div class="item">
              <img src="/assets/images/slider-img-1.jpg" alt="">
            </div><!-- /.item -->
          </div><!-- /.wrapper -->
        </div><!-- /.visiable -->
      </div><!-- /.slider -->
    </div><!-- /.content -->
  </div><!-- /.inner -->
</div><!-- /.summary -->

<#include "/common/footer.ftl"/>
<@js.jquery/>
<@js.app/>
<script>
  $(function(){
    $.slider({
      box: '.slider .visible',
      wrapper: '.slider .wrapper',
      items: '.slider .item',
      itemnum: '1',
      pre: '.slider .prev',
      next: '.slider .next',
      interval: 5000,
      focus: true
    });
  });
</script>
</body>
</html>
</#escape>