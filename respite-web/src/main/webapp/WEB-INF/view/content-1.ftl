<#import "/macro/url.ftl" as url>
<#import "/macro/js.ftl" as js>
<#import "/macro/css.ftl" as css>
<#escape x as x?html>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>家庭档案管理</title>
<@css.app/>
</head>
<body>
<#include "/common/header.ftl"/>

<div class="banner">
  <div class="inner">
    <div class="pic"></div>
    <div class="desc">
      <span class="ic ic-telephone"></span>
      <span class="txt">服务热线：400-888-8888</span>
    </div><!-- /.desc -->
  </div><!-- /.inner -->
</div><!-- /.banner -->

<div class="summary">
  <div class="inner">
    <div class="title">
      <span class="txt">家庭档案管理：就医安排，疾病管理</span>
    </div><!-- /.title -->

    <div class="content mt-80">
      <div class="list type-a clearfix">
        <div class="item">
          <div class="ic ic-pic-01"></div>
          <p class="txt">家庭健康档案的建立与管理</p>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-02"></div>
          <p class="txt">空巢老人档案的建立与管理</p>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-03"></div>
          <p class="txt">导医安排</p>
        </div><!-- /.item -->

        <div class="item">
          <div class="ic ic-pic-04"></div>
          <p class="txt">疾病管理</p>
        </div><!-- /.item -->
      </div><!-- /.list -->
    </div><!-- /.content -->
  </div><!-- /.inner -->
</div><!-- /.summary -->

<#include "/common/footer.ftl"/>
</body>
</html>
</#escape>